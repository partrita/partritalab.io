#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Deepthought'
SITENAME = 'Biohacker'
SITEURL = 'http://partrita.githun.io'

PATH = 'content'

TIMEZONE = 'Asia/Seoul'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = 3

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

MARKUP = ('md', 'ipynb')
PLUGIN_PATHS = ['./plugins']
PLUGINS = ['ipynb.markup']

IGNORE_FILES = [".ipynb_checkpoints"]
#IPYNB_USE_METACELL = True

THEME = "themes/flex"
OUTPUT_PATH = 'public'

#SITELOGO = SITEURL + '/images/profile.png'
#FAVICON = SITEURL + '/images/favicon.ico'
BROWSER_COLOR = '#333'
# ROBOTS = 'index, follow'
#EXTRA_PATH_METADATA = {
#    'extra/custom.css': {'path': 'static/custom.css'},
#}
#CUSTOM_CSS = 'static/custom.css'

MAIN_MENU = True

# Blogroll
LINKS = (
         ('About','#'),
         ('Biohack', 'http://partrita.iptime.org/'),
         ('Portfolio','http://partrita.iptime.org/portfolio'),
         ('Need to config pelicanconf.py', '#'),)

# Social widget
SOCIAL = (('linkedin', 'https://br.linkedin.com/in/partrita/en'),
          ('github', 'https://github.com/partrita'),
          ('gitlab', 'https://gitlab.com/partrita'),
          ('twitter', 'https://twitter.com/partrita'),
          ('rss', '/blog/feeds/all.atom.xml'))

MENUITEMS = (('Archives', '/archives.html'),
             ('Categories', '/categories.html'),
             ('Tags', '/tags.html'),)

COPYRIGHT_NAME = "Taeyoon kim"
COPYRIGHT_YEAR = 2019

CC_LICENSE = {
    'name': 'Creative Commons Attribution-ShareAlike',
    'version': '4.0',
    'slug': 'by-sa'
}